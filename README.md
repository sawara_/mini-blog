# インターンテスト用に作成したブログ

## 使い方

このアプリケーションを動かす場合は，まずはリポジトリを手元にクローンしてください．
その後，次のコマンドで必要になる Ruby Gems をインストールします．

```
$ bundle install --without production
```

その後，データベースへのマイグレーションを実行します．

```
$ rails db:migrate
```

最後に，テストを実行してうまく動いているかどうか確認してください．

```
$ rails test
```

テストが無事に通ったら，Railsサーバーを立ち上げる準備が整っているはずです．

```
$ rails server
```

## 仕様

投稿とそれに対するコメントができるようになっています．  
また，各投稿に対して”いいね”をすることができます．  
投稿，コメントにはそれぞれ140文字までという制限があります．  

## バージョン

- ruby 2.3.0p0 (2015-12-25 revision 53290)  
- Rails 5.0.4

## エラーが発生した場合

*ArgumentError in BlogsController#index*    
*Key must be 32 bytes*  
railsのバージョンに問題がある．そのためGemfileに記載しているRailsのバージョンを5.0.0.1→5.0.1に変更して，以下のコマンドを実行．
```
$ bundle update
```
参照「 http://qiita.com/KTakata/items/7bc8057b3b0ca6759b12 」  2017/07/16(参照日)