class BlogsController < ApplicationController
  
  def index
    @newblog = Blog.new
    @blog = Blog.order('id desc')
    @reply = Reply.all
  end

  def show
    @blogID = params[:id]
    @newreply = Reply.new(blog_id: @blogID)
    @blog = Blog.find(@blogID)
    @reply = Reply.where(blog_id: @blogID)
  end
  
  # 新しい投稿の作成
  def create
    @blog = Blog.new(blog_params)
    if @blog.save
      redirect_to(root_url)
    else
      # 今のところ失敗してもrootに返す仕様
      redirect_to(root_url)
    end
  end
  
  # root(index)からのいいね
  def up_from_index
    @blog = Blog.find(params[:blog][:id])
    @blog.update_attributes(like: @blog.like+1)
    redirect_to(root_path)
  end
  
  # showからのいいね
  def up_from_show
    @blog = Blog.find(params[:blog][:id])
    @blog.update_attributes(like: @blog.like+1)
    redirect_to(blog_path(@blog.id))
  end
  
  # 定義しているだけ
  def edit
  end
  
  # 定義しているだけ
  def update
  end
  
  private
    def blog_params
      params.require(:blog).permit(:body)
    end
end
