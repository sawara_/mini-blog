class RepliesController < ApplicationController
    
    # 新しいコメント作成
    def create
        @reply = Reply.new(reply_params)
        @reply.save
        # @reply.blog_idとして送るが，受け取り方はparams[:id]となる
        redirect_to(blog_path(@reply.blog_id))
    end
    
    private
    def reply_params
      params.require(:reply).permit(:blog_id, :comment)
    end
   
end
