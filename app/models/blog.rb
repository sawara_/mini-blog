class Blog < ApplicationRecord
  
  # body should be valid
  # 投稿文字数を140字までに制限する
  validates(:body, presence: true, length: {maximum: 140})
  
end
