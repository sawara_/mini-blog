class Reply < ApplicationRecord
  
  # add reration
  belongs_to :blog
  
  # comment should be valid
  # 空白文字を許す
  # 投稿文字数を140字までに制限する
  validates(:comment, length: {maximum: 140})
  
end
