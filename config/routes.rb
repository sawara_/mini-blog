Rails.application.routes.draw do
  
  # root
  root 'blogs#index'
  
  # root(index)からのcreate(新しい投稿の作成)
  match '/blogs/index' => 'blogs#create', :via => :post
  # root(index)からのいいね
  match '/blogs/likeup' => 'blogs#up_from_index', :via => :post
  
  # showからのいいね
  match '/blogs/show/likeup' => 'blogs#up_from_show', :via => :post
  
  resources :blogs
  
  resources :replies

end
