require 'test_helper'

class BlogsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @base_title = "InternTest"
    @blog = blogs(:one)
    @newBlog = blogs(:two)
  end
  
  test "should get root(index)" do
    # パスが通っているかのテスト
    get root_path
    assert_response :success
    # タイトルが適合しているかのテスト
    assert_select "title", "#{@base_title}"
  end
  
  test "should get show" do
    get blog_path(@blog.id)
    assert_response :success
    assert_select "title", "#{@base_title}"
  end
  
  test "should post create" do
    post blogs_index_path(@newBlog), params: {
            blog: { body: "Hello World 2",
                    like: "2" }
          }
    assert_redirected_to root_path
  end
  
  test "should post likeup_from_index" do
    post blogs_likeup_path(@newBlog), params: {
            blog: { id: "1"}
          }
    assert_redirected_to root_path
  end
  
  test "should post likeup_from_show" do
    post blogs_show_likeup_path(@newBlog), params: {
            blog: { id: "2"}
          }
    assert_redirected_to blog_path(@newBlog.id)
  end
  
end
