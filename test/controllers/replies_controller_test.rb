require 'test_helper'

class RepliesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @reply = replies(:one)
  end
  
  test "should post create" do
    post replies_path(@reply), params: {
            reply: { blog_id: "1",
                    comment: "Comment Test 1" }
          }
    assert_redirected_to blog_path(@reply.blog_id)
  end
  
end
