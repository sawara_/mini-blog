require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  def setup
    @blog = blogs(:one)
  end

  test "layout links index" do
    get root_path
    assert_template 'blogs/index'
    assert_select "a[href=?]", blog_path(@blog)
  end
  
  test "layout links show" do
    get blog_path(@blog)
    assert_template 'blogs/show'
    assert_select "a[href=?]", root_path
  end

end
