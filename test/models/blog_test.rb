require 'test_helper'

class BlogTest < ActiveSupport::TestCase
  
  # はじめの定義
  def setup
    @blog = Blog.new(id: "1", body: "hello", like: "0")
  end
  
  # はじめの定義が有効化どうか
  test "should be valid" do
    assert @blog.valid?
  end
  
  # 空白文字が無効になっているかのテスト
  test "body should be present" do
    @blog.body = ""
    assert_not @blog.valid?
  end
  
  # 文字制限140文字
  test "body should not be too long" do
    @blog.body = "a" * 141
    assert_not @blog.valid?
  end
  
end
