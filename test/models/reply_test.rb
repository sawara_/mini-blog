require 'test_helper'

class ReplyTest < ActiveSupport::TestCase
  
  # はじめの定義
  def setup
    @reply = replies(:one)
  end
  
  # はじめの定義が有効化どうか
  test "should be valid" do
    assert @reply.valid?
  end
  
  # 空白文字が有効になっているかのテスト
  test "comment should be present" do
    @reply.comment = ""
    assert @reply.valid?
  end
  
  # 文字制限140文字
  test "comment should not be too long" do
    @reply.comment = "a" * 141
    assert_not @reply.valid?
  end
  
  # replyにblog_idが定義されていないとエラーになるかのテスト
  # replies.yml(:three)を定義するとエラーになることで確認
  test "cannot comment have no blog_id" do
  end
  
end
